export declare const create: <Key, Arg, Ret>() => {
    on(k: Key, f: (arg: Arg) => Ret): void;
    off(k: Key): void;
    emit(k: Key, arg: Arg): Ret[];
};
