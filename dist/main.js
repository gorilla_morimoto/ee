export const create = () => {
    const map = new Map();
    return {
        on(k, f) {
            map.set(k, [...(map.get(k) || []), f]);
        },
        off(k) {
            map.delete(k);
        },
        emit(k, arg) {
            return (map.get(k) || []).map(f => f.call(null, arg));
        }
    };
};
